
resource "aws_ecs_cluster" "default" {
  name = var.cluster_name
  setting {
    name  = "containerInsights"
    value = "enabled"
  }  
}

resource "aws_ecs_service" "backend" {
  name = var.backend_service_name

  cluster     = aws_ecs_cluster.default.id
  launch_type = "EC2"

  task_definition = var.backend_task_definition_arn
  desired_count   = var.backend_desired_count
  iam_role        = aws_iam_role.ecs-service-role.arn

  load_balancer {
    target_group_arn = aws_alb_target_group.backend-target-group.arn
    container_name   = var.backend_container_name
    container_port   = var.backend_container_port
  }

  depends_on = [aws_alb_target_group.backend-target-group]
}

resource "aws_ecs_service" "react" {
  name = "react_as_frontend"

  cluster     = aws_ecs_cluster.default.id
  launch_type = "EC2"

  task_definition = var.frontend_task_definition_arn
  desired_count   = var.frontend_desired_count
  iam_role        = aws_iam_role.ecs-service-role.arn

  load_balancer {
    target_group_arn = aws_alb_target_group.frontend-target-group.arn
    container_name   = var.frontend_container_name
    container_port   = var.frontend_container_port
  }

  depends_on = [aws_alb_target_group.frontend-target-group]
}