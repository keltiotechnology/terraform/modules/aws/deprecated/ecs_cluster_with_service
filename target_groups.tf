/*
 * Create target group for ECS backend service (Django)
*/
resource "aws_alb_target_group" "backend-target-group" {
  name     = "backend-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    path                = var.backend_heathcheck_path
    port                = "traffic-port"
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200"
  }

}


/*
 * Create target group for ECS frontend service (React)
*/
resource "aws_alb_target_group" "frontend-target-group" {
  name     = "frontend-tg"
  port     = 8001
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    path                = var.frontend_heathcheck_path
    port                = "traffic-port"
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200"
  }

}

/*
 * Create listener rule for HTTP traffic
*/
resource "aws_alb_listener" "http" {
  load_balancer_arn = var.load_balancer_arn
  port              = "80"
  protocol          = "HTTP"
  depends_on        = [aws_alb_target_group.backend-target-group, aws_alb_target_group.frontend-target-group]  
  
  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.frontend-target-group.arn
  }
}


/*
 * Create listener rule for HTTPS traffic
*/
resource "aws_alb_listener" "https" {
  load_balancer_arn = var.load_balancer_arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.acm_certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.frontend-target-group.arn
  }
}

/*
 * Add listener rule to forward HTTP traffic at path /backend/* to ECS backend service (Django)
*/
resource "aws_alb_listener_rule" "http" {
  listener_arn = aws_alb_listener.http.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.backend-target-group.arn
  }

 # TODO: Add variable for path-based routing condition
  condition {
    path_pattern {
      values = ["/backend/*"]
    }
  }
  depends_on = [aws_alb_listener.http]
}

/*
 * Add listener rule to forward HTTPS traffic at path /backend/* to ECS backend service (Django)
*/
resource "aws_alb_listener_rule" "https" {
  listener_arn = aws_alb_listener.https.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.backend-target-group.arn
  }

  condition {
    path_pattern {
      values = ["/backend/*"]
    }
  }
  depends_on = [aws_alb_listener.https]
}